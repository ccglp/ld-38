﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MainCharacter : MonoBehaviour {
	[SerializeField]
	private float speed; 

	[SerializeField]
	private float forceJump; 




	private UIController uiController; 
	private GameController gameController; 

	private bool canJump = true; 

	private new Transform transform; 
	private Rigidbody2D rb; 

	private float timer = 0; 

	private bool canCheck = true;
    private AudioSource jumpSound; 
	private Vector3 teleportRight, teleportLeft; 

	bool secret_1 = true; 
	bool secret_4 = false;

    public bool Secret_1
    {
        get
        {
            return secret_1;
        }

        set
        {
            secret_1 = value;
        }
    }

    // Use this for initialization
    void Start () {
        jumpSound = GameObject.Find("JumpSound").GetComponent<AudioSource>(); 
		this.transform = this.GetComponent<Transform> (); 
		this.rb = this.GetComponent<Rigidbody2D> (); 
		this.gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController> (); 
		this.uiController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<UIController> (); 
		teleportRight = GameObject.FindGameObjectWithTag ("TR").transform.position; 
		teleportLeft = GameObject.FindGameObjectWithTag ("TL").transform.position; 

	}
	
	// Update is called once per frame
	private void CheckLeftAndRight(){
		float vX = this.rb.velocity.x; 

		if (vX > 0) {
			Vector2 pos = new Vector2 (this.transform.position.x + this.transform.lossyScale.x * 0.5f + 0.01f, this.transform.position.y); 
			RaycastHit2D hit = Physics2D.Raycast (pos, new Vector2 (1, 0), Mathf.Abs(vX/Time.fixedDeltaTime));
			if (hit.collider != null) {
				rb.velocity = new Vector2 (0, rb.velocity.y); 
			}
		} else if (vX < 0) {
			Vector2 pos = new Vector2 (this.transform.position.x -  this.transform.lossyScale.x * 0.5f - 0.01f, this.transform.position.y); 
			RaycastHit2D hit = Physics2D.Raycast (pos, new Vector2 (-1, 0), Mathf.Abs(vX/Time.fixedDeltaTime));
			if (hit.collider != null && !hit.collider.isTrigger) {
				print (hit.collider.name); 
				rb.velocity = new Vector2 (0, rb.velocity.y); 
			}
		}

	}
	void Update () {
		CheckForJump (); 
		this.rb.velocity = new Vector2 (speed * Input.GetAxis ("Horizontal"), rb.velocity.y); 
		//CheckLeftAndRight (); 
		if (Input.GetButtonDown("Jump")) {
			Jump (); 
		}

		timer += Time.deltaTime; 


		if (!secret_1 && Input.anyKeyDown) {
			if (!Input.GetKeyDown (KeyCode.W) && !Input.GetKeyDown (KeyCode.A) && !Input.GetKeyDown (KeyCode.D) && !Input.GetKeyDown (KeyCode.Space)
                && !Input.GetMouseButtonDown(0) && !Input.GetKeyDown(KeyCode.LeftArrow) && !Input.GetKeyDown(KeyCode.RightArrow) && !Input.GetKeyDown(KeyCode.UpArrow)) {
				secret_1 = true; 
				uiController.ShowMobile (1);
				uiController.ScheduleMessage ("Dad's words echoed... There's a time and place for everything, but not now", 2, true); 
				uiController.ScheduleMessage ("But I ain't your dad...", 2, true, () => { 
					DOVirtual.DelayedCall(2,()=>{
					uiController.HideMobile(1); 
					gameController.ShowSecret(1); 
					}); 
				});
			}
		}


	}

	private void CheckForJump(){
		if (canCheck) {
			Vector2 limitDown = new Vector2(this.transform.position.x ,this.transform.position.y - this.transform.lossyScale.y * 0.5f - 0.01f);
			RaycastHit2D hit = Physics2D.Raycast (limitDown, new Vector2 (0, -1), 0.02f); 
			Debug.DrawLine (limitDown, limitDown + new Vector2 (0, -0.02f), Color.red); 
			canJump = hit.collider != null && !hit.collider.isTrigger; 

		}
	}

	void Jump(){
		if (canJump && Time.timeScale > 0.9f) {
            jumpSound.Play(); 
			canCheck = false; 
			DOVirtual.DelayedCall (0.2f, () => {
				canCheck = true; 
			}); 
			rb.AddForce (new Vector2 (0, forceJump)); 
			canJump = false; 
		}
	}

	void OnCollisionEnter2D(Collision2D coll){
		//canJump = true; 
	}
    void OnTriggerStay2D(Collider2D coll)
    {
		if (coll.gameObject.tag == "Upper")
       	 	rb.velocity = new Vector2(rb.velocity.x, 4);

		if (!secret_4 && coll.gameObject.tag == "ShutHouse") {
			if (Input.GetAxisRaw ("Vertical") > 0) {
				secret_4 = true; 
				uiController.ShowMobile (1); 
				uiController.ScheduleMessage ("You can not enter", 1); 
				uiController.ScheduleMessage ("I don't want someone like you", 2); 
				uiController.ScheduleMessage ("Go out.", 3, true, () => {
					DOVirtual.DelayedCall(2, ()=>{
						uiController.HideMobile(1); 
						gameController.ShowSecret(4); 
					}); 
				});
			}
		}
    }

    


	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "TR") {
			this.transform.position = new Vector3 (teleportLeft.x + 6, this.transform.position.y, this.transform.position.z); 
		} 

        else if (coll.gameObject.tag == "Forest_Trigger")
        {
            gameController.ShowSecret(5);
            gameController.DestroyAllPlatforms(true); 
            Destroy(coll.gameObject);
        }
        else if (coll.gameObject.tag == "Hope")
        {
            GameObject.Find("hope_text").GetComponent<HopeText>().Hope = true; 
        }

	}
}
