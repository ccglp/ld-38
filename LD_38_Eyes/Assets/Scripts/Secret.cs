﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 
public class Secret : MonoBehaviour {
	Collider2D coll; 
	Transform target; 
	// Use this for initialization
	void Start () {
		this.coll = this.GetComponent<Collider2D> (); 
		target = GameObject.Find ("Target_Count").transform; 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Player") {
			this.coll.enabled = false; 
			Transform targ = target; 
			Vector3 dir = targ.position - this.transform.position; 
			float distance = dir.magnitude; 
			DOVirtual.DelayedCall (1, () => {
				GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().HandleSecret(); 
				Destroy(this.gameObject); 
			}).OnUpdate(()=>{
				dir = targ.position - this.transform.position; 
				dir.Normalize(); 
				this.transform.position+= dir * distance * Time.deltaTime;
			}); 


		}
	}
}
