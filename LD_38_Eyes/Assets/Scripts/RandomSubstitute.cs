﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSubstitute : MonoBehaviour {
    [SerializeField]
    private GameObject[] prefabs;
	// Use this for initialization
	void Awake () {
        Transform parent = this.transform.parent;

        GameObject aux = ((GameObject)Instantiate(prefabs[Random.Range(0, prefabs.Length)], this.transform.position, this.transform.rotation));
        aux.transform.SetParent(parent);
        aux.transform.localScale = this.transform.localScale;
        Destroy(this.gameObject);  

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
