﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroy : MonoBehaviour {

	[SerializeField]
	private float timeToDestroy = 10; 
	private float timer = 0; 

	public void InitializeParticles(Vector3 shape, bool white = false){
		ParticleSystem partSys = this.GetComponent<ParticleSystem> (); 

		ParticleSystem.ShapeModule shapeMod =  partSys.shape;
        ParticleSystem.MainModule main =  partSys.main;
            
        main.startColor = white ? Color.white : Color.black; 
		shapeMod.box = shape;


	}

	void Update(){
		this.timer += Time.deltaTime; 
		if (this.timer > timeToDestroy) {
			Destroy (this.gameObject); 
		}
	}
}
