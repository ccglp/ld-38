﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaOffset : MonoBehaviour {
	[SerializeField][Range(-1,1)]
	private float offset; 
	// Use this for initialization
	void Start () {
		SpriteRenderer[] rends = this.GetComponentsInChildren<SpriteRenderer>(); 

		for (int i = 0; i < rends.Length; i++)
		{
			rends[i].color = new Color(rends[i].color.r, rends[i].color.g, rends[i].color.b, rends[i].color.a + offset); 
		}
	}
	
	
}
