﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 
using UnityEngine.UI; 

public delegate void MobileCallback (); 

public class UIController : MonoBehaviour {


	private RectTransform mobile; 
	private Message[] uiMessages; 
	private int actualMessage = 0; 
	private float actualDelay = 0; 
	private Vector2 initialMobilePos; 
	private Text secretCount;
    private AudioSource showMobileSound; 

	// Use this for initialization
	void Start () {
        showMobileSound = GameObject.Find("MobileSound").GetComponent<AudioSource>(); 
		DOTween.Init (); 
		DOTween.defaultTimeScaleIndependent = true; 
		mobile = GameObject.Find ("Mobile").GetComponent<RectTransform> (); 
		uiMessages = mobile.GetComponentsInChildren<Message> (); 
		initialMobilePos = mobile.anchoredPosition; 
		secretCount = GameObject.Find ("SecretCount").GetComponent<Text> (); 
		for (int i = 0; i < uiMessages.Length; i++) {
			uiMessages [i].gameObject.SetActive (false); 

		}
	}

	public void SetSecretCount(int count){
		secretCount.text = count.ToString () + " / " + "10"; 
	}

	public void UnvealSecretCount(){
		secretCount.DOColor (Color.white, 1); 
	}
	public void ShowMobile(float time){
        showMobileSound.Play(); 
		Time.timeScale = 0; 
		actualMessage = 0; 
		actualDelay = 0; 
		for (int i = 0; i < uiMessages.Length; i++) {
			uiMessages [i].gameObject.SetActive (false); 
		}
		mobile.gameObject.SetActive (true); 
		mobile.DOAnchorPos (new Vector2 (0, 0), time).SetEase(Ease.OutBounce); 
	}

	public void HideMobile(float delay){
		Time.timeScale = 1; 
		actualMessage = 0; 
		actualDelay = 0; 
		mobile.DOAnchorPos (initialMobilePos, delay).OnComplete(()=>{
			mobile.gameObject.SetActive(false); 
		});
	}

	public void ScheduleMessage(string text, float delay, bool hide = true, MobileCallback call = null){
		uiMessages [actualMessage].gameObject.SetActive (true); 
		uiMessages [actualMessage].SetText (delay + actualDelay, text, call); 
		if (hide && actualDelay > 0) {
			uiMessages [actualMessage].HideAlpha (); 
			int aux = actualMessage; 
			DOVirtual.DelayedCall (actualDelay , () => {
				uiMessages [aux].ShowAlpha ();  
			}); 
		}

		actualDelay += delay; 
		actualMessage++; 
	}

	// Update is called once per frame
	void Update () {
		if (actualDelay > 0) {
			actualDelay -= Time.deltaTime; 
		}
	}
}
