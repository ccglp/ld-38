﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 


public class Suicide : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D coll){
		
		if (coll.gameObject.tag == "Player") {
			GameController gameController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> (); 
			Animator anim = this.GetComponent<Animator> (); 

			anim.SetBool ("Jump", true); 
			SpriteRenderer rend = this.GetComponent<SpriteRenderer>(); 

			DOVirtual.DelayedCall (4f, () => {
				gameController.ShowSecret(3); 
				rend.DOColor(new Color(rend.color.r, rend.color.g, rend.color.b, 0),1).OnComplete(()=>{
					Destroy(this.gameObject); 
				}); 
			}); 
		}

	}
}
