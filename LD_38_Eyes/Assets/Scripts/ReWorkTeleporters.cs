﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct MessageArray
{
    public MessageStruct[] array; 
}
public class ReWorkTeleporters : MonoBehaviour {

    [SerializeField]
    private MessageArray[] messagesByLevel;
    private int index = 0;

    private GameController gameController;
    private TriggerMessage trigg; 
    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        trigg = GameObject.FindGameObjectWithTag("TR").GetComponent<TriggerMessage>(); 
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            gameController.TeleportSetEnabled(true,false); 
            trigg.Messages = messagesByLevel[index].array;
            index++; 
        }
    }
}
