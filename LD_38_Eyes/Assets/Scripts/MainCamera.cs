﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

[System.Serializable]
public struct FollowConstraints
{
    public bool x, y, z; 
}

public class MainCamera : MonoBehaviour {
    [SerializeField]
    private Transform target;
    [SerializeField]
    private float lag = 0.1f;
    [SerializeField]
    private bool autoTargetPlayer;
    [SerializeField]
    private FollowConstraints constraints, constraintsOffset; 
    private Queue<Vector3> queueLag;
    private float timer = 0;
    private Vector3 offSet;

    [SerializeField]
    private float shakeScaleDef = 0.4f;
    [SerializeField]
    private float timeShake = 0.5f;
    [SerializeField]
    private float rangeMaxShake = 1; 

    private Tween shakeTween; 
    private float scaleShake = 0;
    private Vector2 originalPos; 
	// Use this for initialization
	void Start () {
		if (autoTargetPlayer)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
        queueLag = new Queue<Vector3>();
        offSet = new Vector3(constraintsOffset.x ? 0 : this.transform.position.x - target.position.x,
            constraintsOffset.y ? 0 : this.transform.position.y - target.position.y,
            constraintsOffset.z ? 0 : this.transform.position.z - target.position.z);
        originalPos = new Vector2(this.transform.position.x, this.transform.position.y);

    }

    public void ShakeCall()
    {
        if (shakeTween != null)
            shakeTween.Kill(false);

        scaleShake = shakeScaleDef;
        shakeTween =  DOVirtual.DelayedCall(timeShake, () =>
        {
            scaleShake = 0;
            this.transform.DOMoveX(originalPos.x, 0.1f);
           // this.transform.DOMoveY(originalPos.y, 0.1f); 
        });
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        queueLag.Enqueue(target.position + offSet);
        if (timer > lag)
        {
            Vector3 aux = queueLag.Dequeue();
			if (this.transform.position.z <= aux.z)
            this.transform.position = new Vector3(constraints.x ? this.transform.position.x : aux.x,
                constraints.y ? this.transform.position.y : aux.y,
                constraints.z ? this.transform.position.z : aux.z);
            if (scaleShake > 0.01f)
            {
                if (this.transform.position.x > this.originalPos.x + rangeMaxShake)
                {
                    this.transform.position += new Vector3(Random.Range(-scaleShake, 0) * Time.deltaTime, 0, 0);
                }
                else if (this.transform.position.x < this.originalPos.x - rangeMaxShake)
                {
                    this.transform.position += new Vector3(Random.Range(0, scaleShake) * Time.deltaTime, 0,0); 
                }
                else
                {
                    this.transform.position += new Vector3(Random.Range(-scaleShake, scaleShake) * Time.deltaTime, 0, 0); //Random.Range(-scaleShake, scaleShake)*Time.deltaTime, 0);

                }

            }
        }
        else
        {
            timer += Time.deltaTime;
        }
	}
}
