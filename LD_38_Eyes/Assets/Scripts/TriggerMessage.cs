﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

[System.Serializable]
public struct MessageStruct{
	public string text; 
	public float delay; 
	public bool hide;  
}
public class TriggerMessage : MonoBehaviour {
	[SerializeField]
	private MessageStruct[] messages; 
	[SerializeField]
	private bool closeMobileAfter = true; 
	[SerializeField]
	private float mobileCloseDelay = 1.5f; 
	private UIController uiController; 

	[SerializeField]
	private bool destroyTriggerAfter = true; 
	private MobileCallback callback; 
	void Start(){
		uiController = GameObject.FindGameObjectWithTag ("GameController").GetComponent<UIController> (); 
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Player") {
			uiController.ShowMobile (1); 
			for (int i = 0; i < messages.Length; i++) {
				
				if (closeMobileAfter && i == messages.Length - 1) {
					MobileCallback callback = this.callback; 
					uiController.ScheduleMessage (messages [i].text, messages [i].delay, messages [i].hide,()=>{
						DOVirtual.DelayedCall(mobileCloseDelay, ()=>{
							uiController.HideMobile(1);
							DOVirtual.DelayedCall(1, ()=>{
								if (callback != null)
									callback(); 
							}); 

						}); 
					}); 

				} else {
					uiController.ScheduleMessage (messages [i].text, messages [i].delay, messages [i].hide); 
				}

			}
			if (destroyTriggerAfter)
				Destroy (this.gameObject);
			else
				uiController.GetComponent<GameController> ().TeleportSetEnabled (false); 
		}
	}














	public MobileCallback Callback {
		get {
			return callback;
		}
		set {
			callback = value;
		}
	}

    public MessageStruct[] Messages
    {
        get
        {
            return messages;
        }

        set
        {
            messages = value;
        }
    }
}
