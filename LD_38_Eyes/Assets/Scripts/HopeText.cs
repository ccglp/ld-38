﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
 
public class HopeText : MonoBehaviour {
    private bool hope = false;
    private TextMesh text;
    private bool H, O, P, E;
    private bool end = false;
    private CanvasGroup cgWhite;
    private GameController gameController;
    private UIController uiController;

    public bool Hope
    {
        get
        {
            return hope;
        }

        set
        {
            hope = value;
        }
    }

    // Use this for initialization
    void Start () {
        text = this.GetComponent<TextMesh>();
        cgWhite = GameObject.Find("White").GetComponent<CanvasGroup>();
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        uiController = gameController.GetComponent<UIController>(); 
	}
	
	// Update is called once per frame
	void Update () {
        if (hope)
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                H = true; 

            }
            if (Input.GetKeyDown(KeyCode.O))
            {
                O = true;
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                P = true;
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                E = true; 
            }

            string hs = H ? "H" : "_";
            string os = O ? "O" : "_";
            string ps = P ? "P" : "_";
            string es = E ? "E" : "_"; 
            text.text = "Never lose " + hs + os + ps + es; 

            if (H && O && P && E && !end)
            {
                gameController.ShowSecret(9); 
                end = true;
                cgWhite.DOFade(1, 10).OnComplete(() =>
                {
                uiController.ShowMobile(1);
                if (gameController.SecretCount < 4)
                {
                    uiController.ScheduleMessage("Never lose hope", 5);
                    uiController.ScheduleMessage("Or...", 2, true, () =>
                    {
                        DOVirtual.DelayedCall(2, () =>
                        {
                            if (Application.platform == RuntimePlatform.WebGLPlayer)
                            {
                                Application.OpenURL("http://www.google.com");
                            }
                            else
                            {
                                Application.Quit();
                            }
                        });
                    });
                }

                else if (gameController.SecretCount < 10)
                {
                    uiController.ScheduleMessage("Never lose hope", 5);
                    uiController.ScheduleMessage("Or...", 2);
                    uiController.ScheduleMessage("Play again?", 4, true, () =>
                     {
                         DOVirtual.DelayedCall(2, () =>
                         {
                             if (Application.platform == RuntimePlatform.WebGLPlayer)
                             {
                                 Application.OpenURL("http://www.google.com");
                             }
                             else
                             {
                                 Application.Quit();
                             }
                         });
                        
                     });
                }
                else
                {
                    uiController.ScheduleMessage("Congratulations.", 1);
                    uiController.ScheduleMessage("This is a history about unnecessary expectations", 2);
                    uiController.ScheduleMessage("Never lose hope. Don't listen. Or do.", 3);
                        uiController.ScheduleMessage("Do what you want. Thank you for playing!", 8); 
                    }
                });
            }
        }

    }
}
