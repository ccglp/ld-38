﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
	[SerializeField]
	private GameObject particlesDestroy; 


	public void DestroyPlatform(bool white = false){
		ParticleDestroy part = ((GameObject)Instantiate (particlesDestroy, this.transform.position, particlesDestroy.transform.rotation)).GetComponent<ParticleDestroy> (); 
		part.InitializeParticles (this.transform.lossyScale, white); 
	
		Destroy (this.gameObject); 
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
