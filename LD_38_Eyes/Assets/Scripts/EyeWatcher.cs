﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Limits{
	public float xMax, xMin, yMax, yMin; 
	public Limits(float xMax, float xMin, float yMax, float yMin){
		this.xMax = xMax; 
		this.xMin = xMin; 
		this.yMax = yMax; 
		this.yMin = yMin; 
	}
}

public class EyeWatcher : MonoBehaviour {


	private Transform[] eyes; 
	private Limits[] limits; 
	[SerializeField]
	private Transform target; 
	[SerializeField]
	private bool autoTargetPlayer = true; 
	[SerializeField]
	private bool followMouse = false; 
	private Limits limitChar; 
	private Vector2[] limitsMid; 
	private Vector3 startPos; 
	// Use this for initialization
	void Start () {

		if (autoTargetPlayer) {
			target = GameObject.FindGameObjectWithTag ("Player").transform; 
		}


		List<Transform> eyesAux = new List<Transform> (); 

		Transform[] trs = this.transform.GetComponentsInChildren<Transform> (); 

		for (int i = 0; i < trs.Length; i++) {
			if (trs [i].name == "Eye") {
				eyesAux.Add (trs [i]); 
			}
		}

		eyes = eyesAux.ToArray (); 
		limits = new Limits[eyes.Length];
		limitsMid = new Vector2[eyes.Length]; 
		for (int i = 0; i < limits.Length; i++) {
			Transform parent = eyes [i].parent; 
			Vector3 parentScale = parent.lossyScale; 
			float xMax = (parent.position.x + parentScale.x * 0.5f) - eyes[i].transform.lossyScale.x * 0.5f;
			float xMin = (parent.position.x - parentScale.x * 0.5f) + eyes[i].transform.lossyScale.x * 0.5f;
			float yMax = (parent.position.y + parentScale.y * 0.5f) - eyes[i].transform.lossyScale.y * 0.5f;			
			float yMin = (parent.position.y - parentScale.y * 0.5f) + eyes[i].transform.lossyScale.y * 0.5f;
			float y = yMin + ((yMax - yMin) * 0.5f);
			float x = xMin + ((xMax - xMin) * 0.5f); 

			limits [i] = new Limits (xMax, xMin, yMax, yMin); 
			limitsMid [i] = new Vector2 (x, y); 
		}
		limitChar = new Limits (this.transform.position.x + this.transform.lossyScale.x * 0.5f,
			this.transform.position.x - this.transform.lossyScale.x * 0.5f, 
			this.transform.position.y + this.transform.lossyScale.y * 0.5f,
			this.transform.position.y - this.transform.lossyScale.y * 0.5f); 
			
		startPos = this.transform.position; 
	}
	
	// Update is called once per frame
	void Update () {
		//Update Eyes

		Vector3 positionToFollow; 
		Vector3 offSet = this.transform.position - startPos; 
		if (followMouse) {
			positionToFollow = Camera.main.ScreenToWorldPoint (Input.mousePosition); 
		} else {
			positionToFollow = target.position; 
		}
		for (int i = 0; i < eyes.Length; i++) {
			if (positionToFollow.x  > this.limitChar.xMax+ offSet.x) {
				eyes [i].transform.position = new Vector3 (limits [i].xMax + offSet.x, eyes [i].transform.position.y, eyes [i].transform.position.z); 
			} else if (positionToFollow.x < this.limitChar.xMin+ offSet.x ) {
				eyes [i].transform.position = new Vector3 (limits [i].xMin + offSet.x, eyes [i].transform.position.y, eyes [i].transform.position.z); 

			} else {
				eyes [i].transform.position = new Vector3 (limitsMid[i].x + offSet.x, eyes[i].transform.position.y, eyes[i].transform.position.z); 
			}

			if (positionToFollow.y > this.limitChar.yMax+ offSet.y ) {
				eyes [i].transform.position = new Vector3 (eyes [i].transform.position.x, limits [i].yMax + offSet.y, eyes [i].transform.position.z); 
			} else if (positionToFollow.y  < this.limitChar.yMin+ offSet.y) {
				eyes [i].transform.position = new Vector3 (eyes [i].transform.position.x, limits [i].yMin + offSet.y, eyes [i].transform.position.z); 
			} else {
				eyes [i].transform.position = new Vector3 (eyes [i].transform.position.x, limitsMid[i].y + offSet.y, eyes [i].transform.position.z); 
			}
		}

	}


}
