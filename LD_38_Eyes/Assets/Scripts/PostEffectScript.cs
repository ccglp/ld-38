﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 
[ExecuteInEditMode]
public class PostEffectScript : MonoBehaviour {

	[SerializeField]
	private Material mat; 

	void OnRenderImage(RenderTexture src, RenderTexture dest){
		Graphics.Blit (src, dest, mat);

	}

    public void EndDistortion()
    {
        mat.DOFloat(0, "_Distortion", 1).OnComplete(()=>
        {
            mat.SetFloat("_Distortion", 0.05f); 
            this.enabled = false;
            GameObject.FindGameObjectWithTag("Player").GetComponent<MainCharacter>().Secret_1 = false; 
        }); 

    }
}
