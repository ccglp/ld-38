﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 
public class GameController : MonoBehaviour {

	List<TriggerMessage> triggers;
	List<Platform> platforms; 

	Collider2D[] colliders;
    Collider2D reworkColl; 
	List<Secret> secrets; 
	private UIController uiController; 
    private int levelIndex = 1;
    [SerializeField]
    private GameObject[] levels; 

	[SerializeField]
	private Sprite[] spritesChar;

    private CanvasGroup cgTitle; 
	private SpriteRenderer character; 

	private ParticleSystem partSecret; 

	private float timerSecret = 0; 
	int secretCount = 0; 

	private bool secret_2 = false;
    private bool isTitle = true; 
    public int SecretCount
    {
        get
        {
            return secretCount;
        }

        set
        {
            secretCount = value;
        }
    }

    // Use this for initialization
    void Start () {
		uiController = this.GetComponent<UIController> (); 
		triggers = new List<TriggerMessage> (); 
		platforms = new List<Platform> (); 
		colliders = new Collider2D[2];
		secrets = new List<Secret> (); 
		colliders [0] = GameObject.FindGameObjectWithTag ("TR").GetComponent<Collider2D> (); 
		colliders [1] = GameObject.FindGameObjectWithTag ("TL").GetComponent<Collider2D> ();
        reworkColl = GameObject.Find("Reworks").GetComponent<Collider2D>();
        reworkColl.enabled = false; 
		character = GameObject.FindGameObjectWithTag ("Player").GetComponent<SpriteRenderer> (); 

		partSecret = GameObject.Find ("Target_Count").GetComponentInChildren<ParticleSystem> (); 
        SetLists();
        cgTitle = GameObject.Find("Title_Screen").GetComponent<CanvasGroup>(); 
        int i; 

		// FIRST TRIGGER CALLBACK ***************************************************

		triggers[0].Callback = ()=>{
			for (i = 0; i < 3; i++){
				platforms[i].DestroyPlatform(); 
			}

			secrets[0].gameObject.SetActive(true); 

			platforms.RemoveRange(0,3); 
		};
        // **************************************************************************

        secrets[1].gameObject.SetActive(false);
        secrets[2].gameObject.SetActive(false); 


	}

	public void HandleSecret(){
		secretCount++; 
		uiController.SetSecretCount (secretCount); 
		if (secretCount == 1)
			uiController.UnvealSecretCount (); 
		partSecret.Play (); 
	}

    void SetLists()
    {
        triggers.Clear();
        platforms.Clear(); 

        string trigger = "Trigger_";
        string platform = "Platform_";
		string secret = "Secret_"; 
        int i = 0;

        while (GameObject.Find(trigger + i.ToString()) != null)
        {
            triggers.Add(GameObject.Find(trigger + i.ToString()).GetComponent<TriggerMessage>());
            i++;
        }



        i = 0;

        while (GameObject.Find(platform + i.ToString()) != null)
        {
            platforms.Add(GameObject.Find(platform + i.ToString()).GetComponent<Platform>());
            i++;
        }

		i = secrets.Count; 

		while (GameObject.Find (secret + i.ToString ()) != null) {
			secrets.Add (GameObject.Find (secret + i.ToString ()).GetComponent<Secret> ()); 
			i++; 
		}

		for ( i = 0; i < secrets.Count; i++){
			if (secrets[i] != null && i != 1 && i != 2)
				secrets [i].gameObject.SetActive (false); 
		}
    }
	
    public void DestroyAllPlatforms(bool white = false)
    {
        for (int i = 0; i< platforms.Count; i++)
        {
            if (platforms[i] != null)
                platforms[i].DestroyPlatform(white); 
        }
    }


	void Update(){

        if (isTitle && Input.anyKeyDown)
        {
            isTitle = false;
            Camera.main.GetComponent<PostEffectScript>().EndDistortion();
            cgTitle.DOFade(0, 1); 
        }

		if (!secret_2 && !isTitle) {
			timerSecret += Time.deltaTime; 
			if (timerSecret > 2) {
				timerSecret = 0; 
				if (Input.GetJoystickNames ().Length > 0 || Application.platform == RuntimePlatform.WebGLPlayer ) {
					uiController.ShowMobile (1);
                    if (Application.platform != RuntimePlatform.WebGLPlayer)
                    {
                        uiController.ScheduleMessage("You are a true gamer", 1);
                        uiController.ScheduleMessage("But true gamers doesn't use " + Input.GetJoystickNames()[Input.GetJoystickNames().Length - 1], 2, true);
                        uiController.ScheduleMessage("You disgust me.", 2, true, () =>
                        {
                            DOVirtual.DelayedCall(2, () =>
                            {
                                uiController.HideMobile(1);
                            });



                            secret_2 = true;
                            ShowSecret(2);
                        });
                    }

                    else
                    {
                        uiController.ScheduleMessage("You are a true gamer", 1);
                        uiController.ScheduleMessage("But true gamers doesn't use the WEBGL version",2);
                        uiController.ScheduleMessage("You disgust me.", 2, true, () =>
                        {
                            DOVirtual.DelayedCall(2, () =>
                            {
                                uiController.HideMobile(1);
                            });



                            secret_2 = true;
                            ShowSecret(2);
                        });
                    }
				}

            }

        }
	}
    private void NextLevel()
    {
        GameObject.Find("Level_" + (levelIndex - 1).ToString()).SetActive(false);
        GameObject levelAux = ((GameObject)Instantiate(levels[levelIndex], new Vector3(0, 0, 0), Quaternion.identity));
        levelAux.name = "Level_" + levelIndex.ToString();
		levelAux.SetActive (true); 
		character.sprite = spritesChar[Mathf.Clamp (levelIndex, 0, spritesChar.Length - 1)]; 
		if (character.transform.localScale.x < 0.9f) {
			character.transform.localScale = new Vector3 (1, 1, 1); 
		}
        if (levelIndex== 3)
        {
            GameObject.Find("TriggerForest").GetComponent<TriggerMessage>().Callback = () =>
            {
                ShowSecret(6);
                DestroyAllPlatforms(true); 
            };
        }

        if (levelIndex == 4)
        {
            GameObject.Find("Coll_Trigger_Start").GetComponent<TriggerMessage>().Callback = () =>
            {
                ShowSecret(8); 
                
            };
            GameObject.Find("Coll_Trigger_End").GetComponent<TriggerMessage>().Callback = () =>
            {
                ShowSecret(7);

            };

        }
        levelIndex++; 
        SetLists();  
    }


	public void ShowSecret(int number){
		this.secrets [number].gameObject.SetActive (true); 
	}

	public void TeleportSetEnabled(bool value, bool triggerNextLevel = true){
		for (int i = 0; i < colliders.Length; i++) {
			colliders [i].enabled = value; 
		}
        reworkColl.enabled = !value; 
        if (triggerNextLevel)
            NextLevel(); 
	}
}
