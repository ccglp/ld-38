﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustRenderOrder : MonoBehaviour {
    [SerializeField]
    private int offset = -5; 
	// Use this for initialization
	void Start () {
        SpriteRenderer[] rends = this.GetComponentsInChildren<SpriteRenderer>(); 

        for (int i = 0; i < rends.Length; i++)
        {
            rends[i].sortingOrder += offset; 
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
