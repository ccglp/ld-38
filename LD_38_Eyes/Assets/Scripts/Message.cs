﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using DG.Tweening; 

public class Message : MonoBehaviour {

	private Text text; 
	private CanvasGroup cg; 
	// Use this for initialization
	void Start () {
		this.text = this.GetComponentInChildren<Text> (); 	
	}

	private void CheckCanvas(){
		if (cg == null)
			cg = this.GetComponent<CanvasGroup> (); 
	}

	public void HideAlpha(){
		CheckCanvas (); 
		cg.alpha = 0; 
	}

	public void ShowAlpha(){
		CheckCanvas (); 
		cg.alpha = 1; 
	}
	
	public void SetText(float delay, string text, MobileCallback call = null){
		if (this.text == null)
			this.text = this.GetComponentInChildren<Text> ();
		int points = 0; 
		float timer = 0; 
		this.text.text = ""; 
		DOVirtual.DelayedCall (delay, () => {
			this.text.text = text;
		},true).OnUpdate(()=>{
			
			timer+= 1; 
			if (timer > 30){
				timer = 0; 
				points++; 
				if (points > 3)
					points = 0; 
				this.text.text = ""; 
				for (int i = 0; i < points; i++)
					this.text.text += ".";
			}

		}).OnComplete(()=>{
			if (call != null)
				call(); 
		});

	}


}
