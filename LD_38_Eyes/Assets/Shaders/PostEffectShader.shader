﻿Shader "ccglp/SinCosDistortionTime"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Distortion("Distortion", Range(0,0.05)) = 0.02
		_Speed ("Speed", Range(0,25)) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _Distortion;
			float _Speed;  

			fixed4 frag (v2f i) : SV_Target
			{

				
				fixed4 col = tex2D(_MainTex,i.uv + float2(sin(i.vertex.y * _Distortion + _Time[1] * _Speed), cos(i.vertex.x *_Distortion + _Time[1] * _Speed) ) *_Distortion);
				// just invert the colors
				//col = 1 - col;
				return col;
			}
			ENDCG
		}
	}
}
