﻿Shader "ccglp/TransitionShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color("Color", Color) = (0,0,0,0)
		_Timer("Timer", Float) = 1
		_TransTex("TransitionTex", 2D) = "white" {}
		_WaitTime("WaitTime", Float) = 1
		[MaterialToggle] _FadeIn("FadeIn", Float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _TransTex;
			float _Timer; 
			fixed4 _Color; 
			float _WaitTime; 
			float _FadeIn; 

			fixed4 frag (v2f i) : SV_Target
			{

				
				fixed4 col;
				fixed4 transCol = tex2D(_TransTex, i.uv); 

				if (_FadeIn > 0.5){
					if ((transCol.r + (_WaitTime / _Timer)) <( _Time[1] / _Timer)){
						col = tex2D(_MainTex, i.uv);
					}
					else{
						col = _Color; 
					}
				}
				else{
					if ((transCol.r + (_WaitTime / _Timer)) >( _Time[1] / _Timer)){
						col = tex2D(_MainTex, i.uv);
					}
					else{
						col = _Color; 
					}
				}
				return col;
			}
			ENDCG
		}
	}
}
